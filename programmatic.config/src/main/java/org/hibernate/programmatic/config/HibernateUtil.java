package org.hibernate.programmatic.config;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	public static SessionFactory getSessionFactory(){
		Configuration configuration = new Configuration();
		configuration.addResource("Event.hbm.xml");
		configuration.addResource("Person.hbm.xml");
		configuration.setProperties(new Properties());
		
		return configuration.buildSessionFactory();
		
	}

}
