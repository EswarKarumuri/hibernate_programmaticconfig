package org.hibernate.programmatic.config;

import java.util.Date;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.programmatic.config.HibernateUtil;
import org.hibernate.programmatic.domain.Event;
import org.hibernate.programmatic.domain.Person;

public class EventManager {
	public static void main(String[] args) {
		EventManager mgr = new EventManager();
		mgr.createAndStoreEvent("My Event", new Date());
		mgr.createAndStorePerson("Lakshmi", "Karumuri", 27);
		//mgr.addPersonToEvent(Long.valueOf(28), mgr.createAndStoreEvent("My Event", new Date()));
		mgr.getPerson();
		HibernateUtil.getSessionFactory().close();
	}

	private Long createAndStoreEvent(String title, Date theDate) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Event theEvent = new Event();
		theEvent.setTitle(title);
		theEvent.setDate(theDate);
		Long id = (Long)session.save(theEvent);

		session.getTransaction().commit();
		return id;
		
	}
	
	private Long createAndStorePerson(String firstName, String lastName, int age) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Person thePerson = new Person();
		thePerson.setAge(age);
		thePerson.setFirstname(firstName);
		thePerson.setLastname(lastName);
		Long id = (Long) session.save(thePerson);

		session.getTransaction().commit();
		return id;
	}
	
	private void addPersonToEvent(Long personId, Long eventId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Person person = (Person)session.load(Person.class, personId);
		Event event = (Event)session.load(Event.class, eventId);
		person.getEvents().add(event);
		
		session.getTransaction().commit();
	}
	
	private void getPerson(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Person> persons = session.createQuery("from Person where id = 28").list();
		for(Person person:persons){
			System.out.println(person.getEvents().size());
		}
		session.getTransaction().commit();
		
	}
}
 